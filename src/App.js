import './App.css';
import {zebra, dog, cat, cow, chicken} from './Assets/Images/images.js';
import {zebraNoise, dogNoise, catNoise, cowNoise, chickenNoise} from './Assets/Sounds/sounds.js';

const data = {
    items: [
        {
            id: 1,
            name: 'Zebra',
            image: zebra,
            sound: zebraNoise
        },
        {
            id: 2,
            name: 'Dog',
            image: dog,
            sound: dogNoise
        },
        {
            id: 3,
            name: 'Cat',
            image: cat,
            sound: catNoise
        },
        {
            id: 4,
            name: 'Cow',
            image: cow,
            sound: cowNoise
        },
        {
            id: 5,
            name: 'Chicken',
            image: chicken,
            sound: chickenNoise
        },
    ]
}

function playSound(sound) {
    new Audio(sound).play();
}

function SoundBoardItem({image, name, sound}) {
    return (
        <div className="item" onClick={() => playSound(sound)}>
            <img src={image} alt={name}/>
        </div>
    );
}

function SoundBoard() {
    return (
        <div className="soundboard">
            {data.items.map((item) => (
                <SoundBoardItem image={item.image} name={item.name} sound={item.sound}/>
            ))}
        </div>
    );
}

function App() {
    return (
        <SoundBoard/>
    );
}

export default App;
