import catNoise from './cat.mp3';
import chickenNoise from './chicken.mp3';
import cowNoise from './cow.mp3';
import dogNoise from './dog.mp3';
import zebraNoise from './zebra.mp3';

export {catNoise, chickenNoise, cowNoise, dogNoise, zebraNoise};