import zebra from './zebra.webp';
import dog from './dog.webp';
import cat from './cat.webp';
import cow from './cow.webp';
import chicken from './chicken.webp';

export {zebra, dog, cat, cow, chicken};